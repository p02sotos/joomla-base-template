<?php
/**
 * @package		Joomla.Site
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/* The following line loads the MooTools JavaScript Library */
JHtml::_('behavior.framework', true);

$app = JFactory::getApplication();
?>
<?php echo '<?'; ?>xml version="1.0" encoding="<?php echo $this->_charset ?>"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
    <jdoc:include type="head" />

    <!-- LOAD Bootstrap or Foundation CSS -->
    <?php if($this->params->get('templatecolor')== 0) : ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/vendor/foundation/css/foundation.css" type="text/css" media="screen, projection" />
    <?php else: ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/vendor/bootstrap/css/bootstrap.min.css" type="text/css" media="screen, projection" />
    <?php endif; ?>

    <!-- LOAD CUSTOM CSS -->
    <link rel="stylesheet/less"  type="text/css"  href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/style.less" type="text/css" media="screen, projection" />

    <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/less.js"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/vendor/foundation/js/vendor/jquery.js"></script>

    <!-- LOAD bootstrap or Foundation  JAVASCRIPT -->
    <?php if($this->params->get('templatecolor')== 0) : ?>
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/vendor/foundation/js/foundation.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/vendor/foundation/js/foundation/foundation.topbar.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/vendor/foundation/js/vendor/modernizr.js"></script>
    <?php else: ?>
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/vendor/foundation/js/vendor/modernizr.js"></script>
    <?php endif; ?>

    <!-- LOAD CUSTOM JAVASCRIPT -->
    <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/template.js"></script>


</head>
<body>
<!-- Cabecera -->
<header>

    <?php if($this->countModules('template-topleft') or $this->countModules('position-0')) : ?>
        <jdoc:include type="modules" name="template-topleft" style="none" />
        <jdoc:include type="modules" name="position-0" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-topright-3') or $this->countModules('position-0')) : ?>
        <jdoc:include type="modules" name="template-topright-3" style="none" />
        <jdoc:include type="modules" name="position-0" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-topright-1') or $this->countModules('position-1')) : ?>
        <jdoc:include type="modules" name="template-topright-1" style="none" />
        <jdoc:include type="modules" name="position-1" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-topright-2') or $this->countModules('position-2')) : ?>
        <jdoc:include type="modules" name="template-topright-2" style="none" />
        <jdoc:include type="modules" name="position-2" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-nav') or $this->countModules('position-4')) : ?>
        <jdoc:include type="modules" name="template-nav" style="none" />
        <jdoc:include type="modules" name="position-4" style="none" />
    <?php endif; ?>

</header>
<!-- FINAL Cabecera -->
<!-- Contenido Central -->
<div id="content">
    <?php if($this->countModules('template-topcontent1') or $this->countModules('position-5')) : ?>
        <jdoc:include type="modules" name="template-topcontent1" style="none" />
        <jdoc:include type="modules" name="position-5" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-topcontent2') or $this->countModules('position-6')) : ?>
        <jdoc:include type="modules" name="template-topcontent1" style="none" />
        <jdoc:include type="modules" name="position-6" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-topcontent3') or $this->countModules('position-7')) : ?>
        <jdoc:include type="modules" name="template-topcontent1" style="none" />
        <jdoc:include type="modules" name="position-7" style="none" />
    <?php endif; ?>

    <?php if($this->countModules('template-breadcrumbs') or $this->countModules('position-7')) : ?>
        <jdoc:include type="modules" name="template-breadcrumbs" style="none" />
        <jdoc:include type="modules" name="position-7" style="none" />
    <?php endif; ?>
    <!-- LUGAR DONDE VAN LOS COMPONENTES -->
    <div id="messages">
        <div class="large-12 columns">
            <jdoc:include type="message" />
        </div>
    </div>
    <div id="component">
        <div class="large-12 columns">
            <jdoc:include type="component" />
        </div>
    </div>
    <?php if($this->countModules('template-bottomcontent1') or $this->countModules('position-8')) : ?>
        <jdoc:include type="modules" name="template-bottomcontent1" style="none" />
        <jdoc:include type="modules" name="position-8" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-bottomcontent2') or $this->countModules('position-9')) : ?>
        <jdoc:include type="modules" name="template-bottomcontent2'" style="none" />
        <jdoc:include type="modules" name="position-9" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-bottomcontent3') or $this->countModules('position-10')) : ?>
        <jdoc:include type="modules" name="template-bottomcontent3" style="none" />
        <jdoc:include type="modules" name="position-10" style="none" />
    <?php endif; ?>




</div>
<!-- Pie del contenido -->



<!-- FINAL CONTENIDO CENTRAL --><!-- PIE -->
<footer>
    <?php if($this->countModules('template-bottom') or $this->countModules('position-11')) : ?>
        <jdoc:include type="modules" name="template-bottom" style="none" />
        <jdoc:include type="modules" name="position-11" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-bottomleft') or $this->countModules('position-12')) : ?>
        <jdoc:include type="modules" name="atomic-search" style="none" />
        <jdoc:include type="modules" name="position-12" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-bottomcenter') or $this->countModules('position-13')) : ?>
        <jdoc:include type="modules" name="atomic-search" style="none" />
        <jdoc:include type="modules" name="position-13" style="none" />
    <?php endif; ?>

    <?php if($this->countModules('template-bottomright') or $this->countModules('position-14')) : ?>
        <jdoc:include type="modules" name="atomic-search" style="none" />
        <jdoc:include type="modules" name="position-14" style="none" />
    <?php endif; ?>


    <?php if($this->countModules('template-copyright') or $this->countModules('position-15')) : ?>
        <jdoc:include type="modules" name="atomic-search" style="none" />
        <jdoc:include type="modules" name="position-15" style="none" />
    <?php endif; ?>

    &copy;<?php echo date('Y'); ?> <?php echo htmlspecialchars($app->getCfg('sitename')); ?>

</footer>
<!-- FIN DEL PIE -->


<jdoc:include type="modules" name="debug" />
<script>
    $(document).foundation();
</script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
</body>
</html>
